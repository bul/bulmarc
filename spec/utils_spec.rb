require 'date'
require 'marc'

require 'bulmarc'

def fpath(name)
  File.expand_path("../fixtures/#{name}.mrc",__FILE__)
end

describe 'From utils.rb' do
  before do
    @record_suppressed = MARC::Reader.new(fpath 'suppressed').first
    @record_newspaper = MARC::Reader.new(fpath 'newspaper').first
    @record_ejournal = MARC::Reader.new(fpath 'ejournal').first
    @record_book = MARC::Reader.new(fpath 'book_880').first
    @record_no_items = MARC::Reader.new(fpath 'ejournal_no_item_records').first
  end

  it "correctly identifies a suppressed record" do
    is_s = suppressed(@record_suppressed)
    expect(is_s).to be true
  end

  it "correctly identifies a non-suppressed record" do
    is_s = suppressed(@record_newspaper)
    expect(is_s).to be false
  end

  it "correctly identifies an online record" do
    is_online = online(@record_ejournal)
    expect(is_online).to be true
  end

  it "correctly identifies a non-online record" do
    is_online = online(@record_book)
    expect(is_online).to be false
  end

  it "correctly handles record with no item records" do
    is_online = online(@record_no_items)
    expect(is_online).to be false
  end

  it "correctly identifies the record id" do
    rid = record_id(@record_book)
    expect(rid).to eq 'b5526960'
  end

end


describe 'From utils.rb date checks' do

  before do
    @record_book = MARC::Reader.new(fpath 'book_880').first
  end

  it "correctly identifies the last update date" do
    ud = updated_date(@record_book)
    expect(ud.to_s).to eq "2013-04-30 00:00:00 UTC"
  end

  it "correctly identifies returns nil for invalid dates" do
    ds1 = "baddatestring"
    ud = bulmarc_convert_date(ds1)
    expect(ud).to eq nil
  end

  it "correctly handles different catalog date strings" do
    ds1 = "09-01-99"
    ud = bulmarc_convert_date(ds1)
    expect(ud).to eq Date.new(1999, 9, 1)

    ds1 = "12-01-90"
    ud = bulmarc_convert_date(ds1)
    expect(ud).to eq Date.new(1990, 12, 1)

    #Future dates
    ds1 = "09-01-18"
    ud = bulmarc_convert_date(ds1)
    expect(ud).to eq Date.new(2018, 9, 1)
  end
end

describe 'From utils.rb items check' do

  before do
    @record_journal = MARC::Reader.new(fpath 'journal_multiple_items').first
  end

  it "correctly identifies the building codes" do
    codes = bul_buildings(@record_journal)
    #Rock
    expect(codes).to include 'r'
    #Annex
    expect(codes).to include 'q'
  end
end