require 'json'
require 'bulmarc'

def fpath(name, extension='mrc')
  File.expand_path("../fixtures/#{name}.#{extension}",__FILE__)
end

def read_marc_json(raw)
  MARC::Record::new_from_hash(JSON.parse(raw))
end

describe 'Brown MARC Record subclass tests' do
  before do
    fname = fpath('journal')
    raw = IO.read(fname)
    @rec = MARC::Record::new_from_marc(raw)
  end

  it "correctly reads the record" do
    expect(@rec).to be_instance_of(MARC::Record)
  end

  it "correctly returns imprint" do
    expect(@rec.imprint).to eq 'Vails Gate, NY : [Science Publications]'
  end

  it "correctly returns subject headings list" do
    sh = ["Technology > Periodicals.", "Economics > Periodicals.", "Social sciences > Periodicals."]
    expect(@rec.subjects).to eq sh
  end

  it "correctly identifies a toc link" do
    rec = '
    {
      "fields": [
          {
              "856": {
                  "ind1": "4",
                  "subfields": [
                      {
                          "u": "http://example.org/"
                      },
                      {
                          "3": "Table of contents"
                      }
                  ],
                  "ind2": "0"
              }
          }
      ]
    }
    '
    rec = read_marc_json(rec)
    expect(rec.toc_link).to eq "http://example.org/"
  end


  it "correctly doesn't identify a toc link" do
    rec = '
    {
      "fields": [
          {
              "856": {
                  "ind1": "4",
                  "subfields": [
                      {
                          "u": "http://example.org/"
                      },
                      {
                          "z": "Full text"
                      }
                  ],
                  "ind2": "0"
              }
          }
      ]
    }
    '
    rec = read_marc_json(rec)
    expect(rec.toc_link).to be_nil
  end

end


describe 'Notes fields' do
  before do
    #A marc hash containing notes fields we are targeting.
     rec = '
    {
      "fields": [
          {
              "500": {
                  "subfields": [
                      {
                          "a": "Title from cover.",
                          "b": "Second note.",
                          "6": "No display."
                      }
                  ]
              },
              "502": {
                  "subfields": [
                      {
                          "a": "Originally presented as the author\'s thesis, Bonn, 1972."
                      }
                  ]
              },
              "622": {
                  "subfields": [
                      {
                          "a": "Bonn, 1972.",
                          "b": "OK"
                      }
                  ]
              }
          }
      ]
    }
    '
    @rec = rec = read_marc_json(rec)
  end

  it "correctly identifies a general note" do
    expect(@rec.get_note("500")[0]).to eq "Title from cover. Second note."
  end

  it "correctly identifies a dissertation note" do
    #504    $a Includes bibliographical references.
    expect(@rec.get_note("502")[0]).to include("presented as the author")
  end

  it "correctly skips a subfield" do
    expect(@rec.get_note("500")[0]).to eq("Title from cover. Second note.")
  end

  it "correctly doesn't skip subfield 6" do
    expect(@rec.get_note("500", options={exclude:[]})[0]).to eq("Title from cover. Second note. No display.")
  end

  it "correctly joins by a different value" do
    expect(
      @rec.by_tag("622", options={join:";"}
        )[0]).to eq("Bonn, 1972.;OK")
  end

end