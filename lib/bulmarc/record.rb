require 'bulmarc/marc_extractor'

ext = BulMarc::Extractor

STATEMENT_OF_RESPONSIBILITY = ext.new(
  '245c',
  :first => true,
  :trim_punctuation => true
)

IMPRINT = ext.new(
  "260abcdefg:264abc",
  :first=>true
)

SUBJECT_DISPLAY =  ext.new(
  "600abcdfklmnopqrtvxyz:610abfklmnoprstvxyz:611abcdefgklnpqstvxyz:630adfgklmnoprstvxyz:650abcvxyz:651avxyz",
  :trim_punctuation => true,
  :separator => ' > ',
)

#A class to add convenience methods for display MARC information.
module MARC

  class Record
    alias :super_by_tag :each_by_tag

    def apply_extract(name, as_string=false)
      #Call the MARCExtractor defined above.
      values = name.send('extract', self)
      if as_string
          #Join the values by space and return string.
          "#{values.join(" ")}"
      else
          return values
      end
    end

    def imprint
      apply_extract(IMPRINT, as_string=true)
    end

    def statement_of_responsibility
      apply_extract(STATEMENT_OF_RESPONSIBILITY, as_string=true)
    end

    def subjects
      vals = apply_extract(SUBJECT_DISPLAY)
    end

    #Return the first TOC link found.
    def toc_link
      self.each_by_tag('856') do |field|
        sf3 = field['3']
        if sf3 and sf3.downcase.include? 'contents'
          url = field['u']
          return url unless url.nil?
        end
      end
      nil
    end

    #Pass in join to change the value that will join subfields.
    def subfield_joiner(values, exclude, join)
      out = []
      values.each do |sf|
        unless exclude.include?(sf.code)
          out << "#{sf.value}"
        end
      end
      out.reject(&:empty?).join(join)
    end

    def by_tag(tags, options={})
      options = {exclude: [], join: " "}.merge(options)
      out = []
      self.super_by_tag(tags) do |fl|
        out << subfield_joiner(fl.subfields, options[:exclude], options[:join])
      end
      out.compact
    end

    #Logic for display note fields.  SF six and eight excluded
    #by default.  Pass in additional excludes.
    def get_note(tags, options={})
      options = {exclude: ["6", "8"], join: " "}.merge(options)
      by_tag(tags, options=options)
    end

  end

end
