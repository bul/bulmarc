require 'date'

#Returns a string with the local record id
#
#Uses strips the leading . and returns the 8 digit number.
#

def record_id(record)
  record['907']['a'][1..8]
end


#Returns true if a record is suppressed.
#
#Identify whether a given record is suppressed.  Local system uses
#field 998 subfield e with a value of n to indicate the item is
#suppressed.
def suppressed(record)
    f998 = record['998']
    f998.subfields.find do |sf|
        if (sf.code == "e")
            if (sf.value == "n")
                return true
            end
        end
    end
    return false
end


#Returns true if record is available online
#
#Identify whether a given record is available online.
#Uses the location code found in item records.
#Brown location codes beginning with "es" indicate the item
#is available online.
def online(record)
  f945 = record['945']
  if f945.nil?
    return false
  end
  f945.subfields.find do |sf|
    if (sf.code == "l")
      if (sf.value.start_with?("es"))
        return true
      end
    end
  end
  return false
end

#Returns date object
#
#Converts a raw date string from a III MARC record into
#a Ruby date object.  Returns nil if date parsing fails.
def bulmarc_convert_date(datestr)
  begin
    return Date.strptime(datestr, "%m-%d-%y")
  rescue ArgumentError
    #Todo: consider logging bad datestrings.
    return nil
  end
end

#Returns date in UTC format for Solr
#
#https://github.com/sunspot/sunspot/blob/ec64df6a526d738f9f77c039679b344f908d3298/sunspot/lib/sunspot/type.rb#L244
#https://cwiki.apache.org/confluence/display/solr/Working+with+Dates
def solr_date(date)
  return Time.utc(date.year, date.mon, date.mday)
end

#Returns date record was last updated
#
#Converts date string found in MARC 907 b to Ruby date obj.
#Will return nil if date parsing fails.
def updated_date(record)
  begin
    d = bulmarc_convert_date(record['907']['b'])
    return solr_date(d)
  rescue Exception
    #Todo: log record id to find malformed updated dates.
    return nil
  end
end

#Returns an array of building codes
#
#Handle Brown locations using the first
#character of the location code as an indicator.
#For now codes beginnng with "e", online, will
#be skipped.
def bul_buildings(record)
  items = record.fields('945')
  #require 'byebug'; byebug
  if items.nil?
    return nil
  end
  buildings = []
  #require 'byebug'; byebug
  items.each do |item|
    item.subfields.each do |sf|
      if sf.code == 'l'
        buildings << sf.value[0]
      end
    end
  end
  return buildings
end