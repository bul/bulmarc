
##bulmarc

Ruby code for processing MARC records at Brown.  The primary purpose (at the moment) is for displaying MARC records in Blacklight.  This allows this code to be
developed and tested without requiring the Blacklight Rails app.

This code uses the `marc_extractor` code from [Traject](https://github.com/traject-project/traject) to allow for display statements to use the Traject DSL.

Since there isn't much code here, it could be better to just include this directly within the Brown blacklight project under `lib`.  When this gem was started,
the thinking was that there would be more (and complex) MARC processing logic for display.