Gem::Specification.new do |s|
  s.name        = 'bulmarc'
  s.version     = '0.0.4.2'
  s.summary     = "MARC record utilities for Brown University Libaries."
  s.authors     = ["Ted Lawless"]
  s.email       = 'lawlesst@brown.edu'
  s.files       = `git ls-files`.split($/)
  s.require_paths = ["lib"]
  s.license       = 'MIT'
end

